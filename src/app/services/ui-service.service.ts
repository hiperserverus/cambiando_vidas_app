import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UiServiceService {

  constructor( private alertController: AlertController,
    private toastController: ToastController) { }

  async presentAlert( message:string, time:number = 0  ) {
    const alert = await this.alertController.create({
      message: `<ion-icon name="heart" size="large"></ion-icon> ${ message }`,
      mode:'ios',
      cssClass: "center",
      buttons: ['OK'],
    });

    await setTimeout(()=>alert.present(),time);
  }

  async presentToast( message:string ) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      cssClass: "center",
      position: 'top'
    });
    toast.present();
  }
}

