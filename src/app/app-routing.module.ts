import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { UsuarioGuard } from './guards/usuario.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [
  { path: '', loadChildren: () => import('./pages/timeline/timeline.module').then( m => m.TimelinePageModule), 
  pathMatch: 'full',
  //canActivate: [ UsuarioGuard],
  },
  { path: 'timeline', 
  loadChildren: () => import('./pages/timeline/timeline.module').then( m => m.TimelinePageModule),
},


  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule),
    canLoad: [ LoginGuard]
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule),
    canLoad: [ UsuarioGuard]
  },
  {
    path: 'publication-create',
    loadChildren: () => import('./pages/publication-create/publication-create.module').then( m => m.PublicationCreatePageModule),
    canLoad: [ UsuarioGuard]  
  },
  {
    path: 'notifications',
    loadChildren: () => import('./pages/notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'detail-post/:id',
    loadChildren: () => import('./pages/detail-post/detail-post.module').then( m => m.DetailPostPageModule)
  },
  {
    path: 'donations',
    loadChildren: () => import('./pages/donations/donations.module').then( m => m.DonationsPageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./pages/list/list.module').then( m => m.ListPageModule)
  },
  {
    path: 'notification-create',
    loadChildren: () => import('./pages/notification-create/notification-create.module').then( m => m.NotificationCreatePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
