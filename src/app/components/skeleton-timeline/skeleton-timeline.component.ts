import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton-timeline',
  templateUrl: './skeleton-timeline.component.html',
  styleUrls: ['./skeleton-timeline.component.scss'],
})
export class SkeletonTimelineComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

}
