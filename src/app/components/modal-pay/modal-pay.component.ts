import { Component, OnInit, Input } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-modal-pay',
  templateUrl: './modal-pay.component.html',
  styleUrls: ['./modal-pay.component.scss'],
})

export class ModalPayComponent implements OnInit {


  @Input()serviceSelected: any;
  @Input()monedaSend: any;
  @Input()monedaRecepter: any;
  @Input()paymentMethodSelected: any;
  @Input()amountSend: any;
  @Input()rateSelected: any;
  @Input()paymentMethodsArray: any;

  post: any;
  postId: any = null;

  depositAccountsArray: any [];

  constructor( private modalCtrl: ModalController,
    private _postService: PostsService) { 
  }

  ngOnInit() {
    console.log('SERVICE_SELECTED: ', this.serviceSelected );
    console.log('MONEDA_SEND: ', this.monedaSend );
    console.log('MONEDA_RECEPT: ', this.monedaRecepter );
    console.log('PAYMENT_METHOD_SELECTED: ', this.paymentMethodSelected );
    console.log('AMOUNT_SEND: ', this.amountSend );
    console.log('RATE_SELECTED:  ', this.rateSelected );
    console.log('PAYMENTS_METHODS_ARRRAY:  ', this.paymentMethodsArray );

    this.postId = this.serviceSelected.product_id;

          this._postService.getPost(this.postId).subscribe(resp => {

            console.log('PRODUCT', resp['product']);

            this.post = resp['product'];

            console.log('POST', this.post);

          });



  }

  close() {
    this.modalCtrl.dismiss();
  }

  valor( value: any ) {

  }


  libreria() {
    
  }


}
