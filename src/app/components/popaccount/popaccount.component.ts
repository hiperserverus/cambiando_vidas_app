import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-popaccount',
  templateUrl: './popaccount.component.html',
  styleUrls: ['./popaccount.component.scss'],
})
export class PopaccountComponent implements OnInit {

  @Input() optionsArray: any[];
  @Input() selectedClicked: any;



  constructor( private popoverController: PopoverController ,
   private _postService: PostsService) { 
    
  }

  ngOnInit() {
    console.log('selectedClicked', this.selectedClicked);
  }

  optionClick( option:any ) {

    console.log('OptionClicked', option);

    this._postService.optionSelectedClick( option, this.selectedClicked);

    this.popoverController.dismiss();

  }

}
