import { Component, OnInit, Input } from '@angular/core';

import { PopoverController } from '@ionic/angular';
import { PopinfoComponent } from '../popinfo/popinfo.component';
import { User } from '../../interfaces/interfaces';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() titulo:string;
  userLoged: User;
  level: string = 'any';
  status:string = 'unloged';

  constructor( private popoverController: PopoverController,
              private _usersService: UsersService) { }

  ngOnInit() {

    this.getStatusLevel();



  }

  async mostrarPop( evento ) {

     await this.getStatusLevel();

    const popover = await this.popoverController.create({
      component: PopinfoComponent,
      componentProps:{level:this.level , status:this.status},
      event: evento,
      mode: 'ios',
      backdropDismiss: true
    });

    await popover.present();

    // const {data} = await popover.onDidDismiss();
    const {data} = await popover.onWillDismiss();
  }

 async getStatusLevel() {
    await this._usersService.getUsuario().then( data => {

      this.userLoged = data;
  
      if ( this.userLoged.name != undefined ) {
  
        console.log('USENTER', data);
  
      if(this.userLoged.admin == 'true') {
          this.level = 'admin'
          this.status = 'loged'
        }else {
          this.level = 'any'
          this.status = 'loged'
        }
  
  
      }

      console.log('USOUT', this.userLoged);
  
    })
    .catch( err => {
      this.level = 'any'
      this.status = 'unloged'
    });
  }

}
