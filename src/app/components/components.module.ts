import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { PostComponent } from './post/post.component';
import { PopinfoComponent } from './popinfo/popinfo.component';
import { PipesModule } from '../pipes/pipes.module';
import { ModalPayComponent } from './modal-pay/modal-pay.component';
import { SkeletonTimelineComponent } from './skeleton-timeline/skeleton-timeline.component';
import { SkeletonDetailpostComponent } from './skeleton-detailpost/skeleton-detailpost.component';
import { SkeletonListComponent } from './skeleton-list/skeleton-list.component';
import { PopaccountComponent } from './popaccount/popaccount.component';



@NgModule({
  entryComponents: [ModalPayComponent, PopaccountComponent],
  declarations: [ HeaderComponent,
     PostsComponent, 
     PostComponent, 
     PopinfoComponent, 
     PopaccountComponent,
     ModalPayComponent, 
     SkeletonTimelineComponent,
    SkeletonDetailpostComponent,
     SkeletonListComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    PipesModule
  ],
  exports: [
    HeaderComponent,
    PostsComponent,
    PostComponent,
    PopinfoComponent,
    PopaccountComponent,
    ModalPayComponent,
    SkeletonTimelineComponent,
    SkeletonDetailpostComponent,
    SkeletonListComponent
  ]
})
export class ComponentsModule { }
