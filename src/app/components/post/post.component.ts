import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../../interfaces/interfaces';
import { PostsService } from '../../services/posts.service';
import { NavController} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { environment } from '../../../environments/environment';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

const site = environment.site;

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {

  @Input() post: any = {};
  // img1:string = "/assets/perro-1.jpg";
  // img2:string = "/assets/perro-2.jpg";
  // img3:string = "/assets/perro-3.jpg";
  donar:string = site;

  postImages:any[] = [];
  purgarImgs:boolean = false;

  slideSoloOpts = {
    allowSlideNext: false,
    allowSlidePrev: false
  }

  cortar = 80;


  constructor( private _postsService: PostsService,
              private  navController: NavController,
              private _storage: Storage,
            private _socialSharing: SocialSharing
              ) { }

  ngOnInit() {
    // this.postImages = [...this.post.image];
    this._postsService.purgarImagenes.subscribe(purgar =>  {
      this.purgarImgs = purgar;
      if ( purgar ) {
        this.postImages = [];
      }
    });
   
    this.imagesCount(this.post.image);
    console.log('IMAGPOST', this.postImages);
  }
  
  imagesCount( img:string ) {

    if(img.includes(',')) {
      console.log('ENTRO', img.split(','));
      this.postImages.push(...img.split(','));
    }else{
      console.log('NO ENTRO');
     this.postImages.push(img);
    }

  }

 async verDetalle() {

  // const modal = await this.modalController.create({
  //   component: DetallePostComponent,
  //   componentProps: {
  //     post: this.post,
  //     imgs: this.postImages
  //   }
  // });

  // modal.present();

//  this._postsService.detailChannel(this.post, this.postImages);

//   this.navController.navigateRoot('/detail-post');



  let post = Object.values(this.post);
  let imgs = this.postImages.toString();

  this._storage.remove('detailPost');
  this._storage.set('detailPost', post);

  this._storage.remove('detailImages');
  this._storage.set('detailImages', imgs);




  this.navController.navigateRoot(`/detail-post/${ this.post.id }`, { animated: true });

 }

 compartir() {
   this._socialSharing.share(`${this.post.name }
   
   ${this.post.description}`);
 }
 
}
