

export interface PostsResponse {
  products: Posts;
}

export interface Posts {
  current_page: number;
  data: Post[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
}

export interface Post {
  id?: number;
  name?: string;
  description?: string;
  quantity?: number;
  status?: string;
  image?: string;
  seller_id?: number;
  created_at?: string;
  updated_at?: string;
  deleted_at?: any;
}


export interface User {
  id?: string,
  dni?: string,
  name?: string,
  image?: string,
  email?: string,
  phone?: string,
  admin?: string,
  password?: string,
  password_confirmation?: string
}

export interface Notification {
  title?: string,
  message?: string,
  postId?: string,
  nameImage?: string
}