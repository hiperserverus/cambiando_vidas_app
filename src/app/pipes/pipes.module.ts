import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomSanitizerPipe } from './dom-sanitizer.pipe';
import { ImageSanitizerPipe } from './image-sanitizer.pipe';
import { ImagenPipe } from './imagen.pipe';
import { CapitalizadoPipe } from './capitalizado.pipe';
import { UrlPipe } from './url.pipe';



@NgModule({
  declarations: [DomSanitizerPipe, ImageSanitizerPipe, ImagenPipe, UrlPipe, CapitalizadoPipe],
  exports: [
    DomSanitizerPipe,
    ImageSanitizerPipe,
    ImagenPipe,
    UrlPipe,
    CapitalizadoPipe
  ]
})
export class PipesModule { }
