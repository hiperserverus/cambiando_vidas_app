import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'url'
})
export class UrlPipe implements PipeTransform {

  constructor( private _domSanatizer: DomSanitizer) {}

  transform( url: string): any {

    url = url + "&output=embed";

    return this._domSanatizer.bypassSecurityTrustResourceUrl(url);
  }

}