import { Component, OnInit } from '@angular/core';
import { Notification } from 'src/app/interfaces/interfaces';
import { UsersService } from 'src/app/services/users.service';
import { UiServiceService } from '../../services/ui-service.service';

@Component({
  selector: 'app-notification-create',
  templateUrl: './notification-create.page.html',
  styleUrls: ['./notification-create.page.scss'],
})
export class NotificationCreatePage implements OnInit {

  titulo:string = 'Notificación';


  notification: Notification = {
    title: '',
    message: '',
  };

  constructor( private _usersService: UsersService, 
              private _uiService: UiServiceService) { }

  ngOnInit() {

  }

 async  newNotification() {

  console.log('llamo');

  await this._usersService.notificationCreate(this.notification).then(resp => {
    this._uiService.presentToast('Notificacion enviada a todos los usuarios¡¡');
  })
  .catch( err => {
    this._uiService.presentToast('No se pudo enviar la notificacion');
  } ) ;

  this.notification = {
    title: '',
    message: '',
  };

  

  }

}
