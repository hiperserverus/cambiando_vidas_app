import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotificationCreatePage } from './notification-create.page';

const routes: Routes = [
  {
    path: '',
    component: NotificationCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationCreatePageRoutingModule {}
