import { Component, OnInit, ApplicationRef } from '@angular/core';
import { PushService } from '../../services/push.service';
import { OSNotificationPayload } from '@ionic-native/onesignal/ngx';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  titulo:string = 'Notificaciones'
  mensajes: OSNotificationPayload[] = [];

  constructor( private _pushService: PushService,
              private applicationRef: ApplicationRef ) { }

  ngOnInit() {
    // this.mensajes = this._pushService.mensajes;

    this._pushService.pushListener.subscribe( noti => {
        this.mensajes.unshift( noti );
        this.applicationRef.tick();
    });
  }

  async ionViewWillEnter() {

    console.log('WILL ENTER');
    this.mensajes = await this._pushService.getMensajes();

  }

  async limpiarNotificaciones() {
    await this._pushService.borrarMensajes();
    this.mensajes = [];
  }

}
