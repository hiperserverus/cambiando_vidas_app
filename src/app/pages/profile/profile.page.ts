import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { User } from '../../interfaces/interfaces';
import { NgForm } from '@angular/forms';
import { UiServiceService } from '../../services/ui-service.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

declare var window: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  titulo:string = 'Perfil';
  userLoged: User = {
    dni: '',
    name: '',
    email: '',
    phone: ''
  }
  precargedImg:any;

  constructor( private _usersService: UsersService,
    private _uiService: UiServiceService,
    private camera: Camera) { }

  ngOnInit() {
    this._usersService.getUsuario().then(data => {
      this.userLoged = data;
    });
    this.precargedImg = null;
  }

  updateUser(fUpdated: NgForm) {

    if (fUpdated.invalid) {
      return;
    }
    console.log('UserForUpfdate', this.userLoged);


    this._usersService.update(this.userLoged)
   .then( resp => {
    this._usersService.getUsuario().then(data => {
      this.userLoged = data;
    });
    this._uiService.presentToast('Usuario actualizado exitosamente!');
   }) 
   .catch( err => {
    this._uiService.presentToast('No se pudo actualizar el usuario!');
    

   });


  }

  async libreria () {

    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    };

    await this.procesar( options );


  }

    procesar( options: CameraOptions ) {

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
     
     //  let base64Image = 'data:image/jpeg;base64,' + imageData;
 
     const img = window.Ionic.WebView.convertFileSrc( imageData );
     console.log('IMAGEN_DATA',imageData);
     console.log('LA IMAGEN',img);
      this.precargedImg = img;

    this._usersService.subirAvatar( imageData )
    .then( resp => {
      this._uiService.presentToast('Imagen actualizada!');
      this.userLoged.image = '';
      this.precargedImg = null;
      this._usersService.getUsuario().then(data => {
        this.userLoged = data;
      });

     }) 
     .catch( err => {
      this._uiService.presentToast('No se pudo subir la imagen!');
      
  
     });


    

     }, (err) => {
      // Handle error
     });
  }

}
