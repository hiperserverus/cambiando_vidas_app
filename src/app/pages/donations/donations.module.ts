import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DonationsPageRoutingModule } from './donations-routing.module';

import { DonationsPage } from './donations.page';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DonationsPageRoutingModule,
    PipesModule
  ],
  declarations: [DonationsPage]
})
export class DonationsPageModule {}
