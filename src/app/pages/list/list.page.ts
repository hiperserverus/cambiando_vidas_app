import { Component, OnInit } from '@angular/core';
import { User } from '../../interfaces/interfaces';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  titulo: String = 'Usuarios';
  users: User[] = [];
  habilitado:boolean = true;
  skeleton:boolean = true;

  constructor( private _usersService: UsersService) { }

  ngOnInit() {
    this.skeleton = true;
    this.loadData();

    this._usersService.nuevoUser.subscribe(user => {
        this.users.unshift( user );
    });
  }

  loadData( event?, pull: boolean = false) {

    this._usersService.getUsers( pull ).subscribe( resp => {
      this.skeleton = false;
      console.log('LOS USUARIOS', resp);
      this.users.push(...resp['users'].data);

      if( event ) {
        event.target.complete();

        if ( resp['users'].data.length === 0){
              this.habilitado = false;
        }
      }


    });
  }

  doRefresh( event ) {
    this.loadData(event, true);
    this.habilitado = true;
    this.users = [];
}

}
