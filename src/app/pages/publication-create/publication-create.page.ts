import { Component, OnInit } from '@angular/core';
import { Post, Notification } from 'src/app/interfaces/interfaces';
import { PostsService } from '../../services/posts.service';
import { UsersService } from '../../services/users.service';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransferObject} from '@ionic-native/file-transfer/ngx';

declare var window: any;

@Component({
  selector: 'app-publication-create',
  templateUrl: './publication-create.page.html',
  styleUrls: ['./publication-create.page.scss'],
})
export class PublicationCreatePage implements OnInit {
  

  titulo:string = 'Nueva Publicacion';
  tempImages: string[] = [];
  imagesData: string[] = [];
  publication: Post = {
    name: '',
    description: ''
  };

  newNotification: Notification = {
    title: '',
    message: '',
  };

  constructor(private _postService: PostsService, 
    private _usersService: UsersService,
    // private _navController: NavController,
    private _route: Router,
    private camera: Camera) { }

  ngOnInit() {
   this._usersService.getUsuario();
    
  }

  async publicationCreate() {

          const created = await this._postService.postCreate(this.publication, this.imagesData);
          
          // CALL FUNTION FOR CREATE AND SEND NEW NOTIFICATION ALL USERS
          this.newNotification = {
            title: this.publication.name,
            message: this.publication.description
          }
          
          await this._usersService.notificationCreate(this.newNotification).then(resp => {
            console.log('Notificacion enviada a todos los usuarios¡¡');
          })
          .catch( err => {
            console.log('No se pudo enviar la notificacion');
          } ) ;

          this.publication = {
            name: '',
            description: ''
          };
          this.tempImages = [];
          this.imagesData = [];
          // this._navController.navigateRoot('/timeline');
          this._route.navigateByUrl('/timeline');

  }

  camara() {

    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.CAMERA
    };
    
    this.procesar( options );

  }
 
  libreria () {

    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    };

    this.procesar( options );

  }

  procesar( options: CameraOptions ) {

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
     
     //  let base64Image = 'data:image/jpeg;base64,' + imageData;
 
     const img = window.Ionic.WebView.convertFileSrc( imageData );
     console.log('IMAGEN_DATA',imageData);
     console.log('LA IMAGEN',img);

    //  this._postService.subirImagen( imageData );
 
     this.tempImages.push( img );
     this.imagesData.push( imageData );
      
 
     }, (err) => {
      // Handle error
     });
  }

  removeImage(i) {
    this.tempImages.splice(i, 1);
    this.imagesData.splice(i, 1);
  }


}
