import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicationCreatePage } from './publication-create.page';

const routes: Routes = [
  {
    path: '',
    component: PublicationCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicationCreatePageRoutingModule {}
